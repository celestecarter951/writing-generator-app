import React from "react";
import ReactDOM from "react-dom/client";

// MUI
import { Box, Toolbar } from "@mui/material";

// Navigation Layout
import TopBar from "./shared/Navigation/TopBar";
import Sidebar from "./shared/Navigation/Sidebar";

// Pages
import CharacterProfile from "./pages/CharacterPage/CharacterProfile";

const App = () => (
  <Box sx={{ display: 'flex'}}>
    <TopBar />
    <Sidebar />
    <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
      <Toolbar />
      <CharacterProfile />
    </Box>
  </Box>
);

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
