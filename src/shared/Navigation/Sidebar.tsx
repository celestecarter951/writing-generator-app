import React from 'react';

import { Drawer, Box, List, ListItem, ListItemButton, ListItemText, Toolbar, Typography } from '@mui/material';

const CHARACTERS = [
  {
    id: 'u1',
    name: 'Zalmir Dyrus'
  },
  {
    id: 'u2',
    name: 'Gwendolyn Cormel'
  },
  {
    id: 'u3',
    name: 'Valen Kadwell'
  },
  {
    id: 'u4',
    name: 'Abel'
  },
  {
    id: 'u5',
    name: 'Yori'
  },
  {
    id: 'u6',
    name: 'Atticus'
  },
];

const Sidebar = () => {
  return (
    <Drawer variant='permanent' anchor='left' sx={{
      width: 240,
      flexShrink: 0,
      '& .MuiDrawer-paper': {
        width: 240,
        boxSizing: 'border-box'
      }
    }}>
      <Toolbar />
      <Typography variant="h6"
        sx={{
          fontWeight: 'bold',
          paddingTop: (theme) => theme.spacing(2),
          paddingLeft: (theme) => theme.spacing(2) }}
      >Character Profiles</Typography>
      <Box sx={{ overflow: 'auto'}} >
      <List component='nav' aria-label='character list'>
        {CHARACTERS.map(character => (
          <ListItem key={character.id} disablePadding>
            <ListItemButton>
              <ListItemText primary={character.name} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      </Box>
    </Drawer>
  );
};

export default Sidebar; 
