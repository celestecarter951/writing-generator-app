import React from "react";

// MUI
import { AppBar, Button, Box, Toolbar, Typography } from "@mui/material";

const TopBar = () => {
  return (
    <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1}}>
      <Toolbar>
        <Typography variant="h5" noWrap component="div" sx={{ flexGrow: 1, maxWidth: 240}}>
          Generator 
        </Typography>
        <Box sx={{ flexGrow: 1}}>
          <Button color="inherit">New Character</Button>
        </Box>
        <Button color="inherit">Login</Button>
        <Button color="inherit">Sign up</Button>
      </Toolbar>
    </AppBar>
  )
}

export default TopBar;