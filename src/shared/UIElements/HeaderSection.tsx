import React from "react";

interface HeaderSectionProps {
  title: string;
}

const HeaderSection = ({ title }: HeaderSectionProps) => {
  return <h2>{title}</h2>;
};

export default HeaderSection;
