import React from "react";

// MUI
import TextField from "@mui/material/TextField";

interface InputGeneratorFieldProps {
  label: string;
  value: string;
  onChange: (value: string) => void;
}

const InputGeneratorField = ({
  label,
  value,
  onChange,
}: InputGeneratorFieldProps) => {
  return (
    <TextField
      label={label}
      value={value}
      onChange={(event) => onChange(event.target.value)}
      variant="standard"
      size="small"
    />
  );
};

export default InputGeneratorField;
