import React, { useState } from "react";

// Shared
import HeaderSection from "../../shared/UIElements/HeaderSection";
import InputGeneratorField from "../../shared/FormElements/InputGeneratorField";

// Front-end dummy data
const INFO_DATA = {
  name: "Zalmir Dyrus",
  age: "18-24",
  magic: "none",
  residence: "Pricalus City",
};

const CharacterProfile = () => {
  const [info, useInfo] = useState(INFO_DATA);

  const updateValue = (newValue: string) => {
    useInfo(prevInfo => {
      return { ...info, name: newValue }
    })
  };

  return (
    <div>
      <InputGeneratorField
        label="Name"
        value={info.name}
        onChange={updateValue}
      />
      <HeaderSection title="Info" />
      <InputGeneratorField
        label="Age"
        value={info.age}
        onChange={(newValue) => useInfo({ ...info, age: newValue })}
      />
      <InputGeneratorField
        label="Magic"
        value={info.magic}
        onChange={(newValue) => useInfo({ ...info, magic: newValue })}
      />
      <InputGeneratorField
        label="Residence"
        value={info.residence}
        onChange={(newValue) => useInfo({ ...info, residence: newValue })}
      />
      <HeaderSection title="Attributes" />
      <HeaderSection title="Mannerisms" />
      <HeaderSection title="Character Arc" />
      <HeaderSection title="Relationships" />
    </div>
  );
};

export default CharacterProfile;
